# Terraform Module for Tenable.io's Nessus Scanner on AWS (archived: Wiz is onboarded instead of Tenable)

[Wiz.io readiness review in progress](https://gitlab.com/gitlab-com/gl-infra/readiness/-/merge_requests/206)

## What is this?

This module installs the Tenable.io Nessus Scanner onto an Amazon Linux 2 instance in AWS. The module leverages `cloud-init` to download a one-liner script and link using our account's linking key.

## What is Terraform?

[Terraform](https://www.terraform.io) is an infrastructure-as-code tool that greatly reduces the amount of time needed to implement and scale our infrastructure. It's provider agnostic so it works great for our use case. You're encouraged to read the documentation as it's very well written.

## Usage

In `tenable.io`'s web UI, grab your linking key from [the **Scans > Scanners** page](https://cloud.tenable.com/app.html#/scans/scanners).

### Example

```terraform
module "nessus_scanner" {
  source  = "ops.gitlab.net:gitlab-com/aws-nessus-scanner/aws
  version = "4.0.0"

  scanner_name        = "My AWS Nessus Scanner"
  tenable_linking_key = "pvwk5qf5bwsuperfakekeypqv3zcovanqnuawebmv23rj9fofsdcul7aaa"
  vpc_id              = "vpc-31896b55"
  subnet_id           = "subnet-4204d234"
  instance_type       = "t3.xlarge"
  instance_name       = "my-nessus-scanner"

  instance_tags = {
    Role        = "security-scanner"
    Projects    = "tenable"
  }
}
```

<!-- BEGIN_TF_DOCS -->
## Requirements

| Name | Version |
|------|---------|
| <a name="requirement_terraform"></a> [terraform](#requirement\_terraform) | >= 1.0 |
| <a name="requirement_aws"></a> [aws](#requirement\_aws) | >= 4.55.0 |
| <a name="requirement_cloudinit"></a> [cloudinit](#requirement\_cloudinit) | >= 2.3.2 |

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | >= 4.55.0 |
| <a name="provider_cloudinit"></a> [cloudinit](#provider\_cloudinit) | >= 2.3.2 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_eip.nessus-scanner-eip](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/eip) | resource |
| [aws_iam_instance_profile.nessus-server-profile](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_instance_profile) | resource |
| [aws_iam_role.nessus-server-role](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_role_policy_attachment.nessus-ec2-read-only](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_iam_role_policy_attachment.nessus_ssm_policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment) | resource |
| [aws_instance.nessus-scanner](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/instance) | resource |
| [aws_security_group.nessus-security-group](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group) | resource |
| [aws_security_group_rule.nessus-allow-outbound](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/security_group_rule) | resource |
| [aws_ami.nessus-image](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/ami) | data source |
| [aws_iam_policy_document.nessus-instance-assume-role-policy](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [cloudinit_config.init](https://registry.terraform.io/providers/hashicorp/cloudinit/latest/docs/data-sources/config) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_instance_name"></a> [instance\_name](#input\_instance\_name) | The name of the instance as it appears in the aws instance list. Overrides any name passed in instance\_tags. | `string` | `null` | no |
| <a name="input_instance_tags"></a> [instance\_tags](#input\_instance\_tags) | A map of tags to apply to the instance | `map(string)` | `{}` | no |
| <a name="input_instance_type"></a> [instance\_type](#input\_instance\_type) | The type of instance, e.g. m3.large, c3.2xlarge, etc. to be spun up | `string` | `"m5.xlarge"` | no |
| <a name="input_nessus_version"></a> [nessus\_version](#input\_nessus\_version) | Version of the Nessus scanner to install. | `string` | `"latest"` | no |
| <a name="input_scanner_name"></a> [scanner\_name](#input\_scanner\_name) | The name of your Nessus scanner as it will appear in the Tenable.io web UI. Defaults to the AWS instance name. | `string` | `null` | no |
| <a name="input_subnet_id"></a> [subnet\_id](#input\_subnet\_id) | Subnet in which the server and related objects will be created | `string` | n/a | yes |
| <a name="input_tenable_linking_key"></a> [tenable\_linking\_key](#input\_tenable\_linking\_key) | The linking code from tenable.io — Go to Scans > Scanners in the web UI and find the Linking Key there | `string` | n/a | yes |
| <a name="input_use_eip"></a> [use\_eip](#input\_use\_eip) | Whether or not to use an Elastic IP address with the Nessus scanner. Defaults to true because the documentation says it is required. | `bool` | `true` | no |
| <a name="input_vpc_id"></a> [vpc\_id](#input\_vpc\_id) | The VPC with which security groups will be associated | `string` | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_security_group_id"></a> [security\_group\_id](#output\_security\_group\_id) | n/a |
<!-- END_TF_DOCS -->

## Contributing

Please see [CONTRIBUTING.md](./CONTRIBUTING.md).

## License

See [LICENSE](./LICENSE).
